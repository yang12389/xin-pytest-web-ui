# coding=utf-8
from base.base import *
from base.base import BasePage
from pagelocators.login_locs import LoginLocator as LL

class LoginBusiness(BasePage):

	def login(self,username, password):
		self.input_text(LL.username, text=username,clear=2,doc="输入用户名")
		self.input_text(LL.password, text=password, doc="输入密码")
		self.click_element(LL.login_button, doc="点击登陆按钮")





