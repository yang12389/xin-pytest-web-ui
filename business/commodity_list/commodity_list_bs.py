# coding=utf-8
from base.base import *
from base.base import BasePage
from pagelocators.commodity_locs.commodity_list_page import CommodityListPage as CLP

class CommodityListBusiness(BasePage):

    """商品名称搜索功能"""
    def enter_search(self,text):
        time.sleep(3)
        self.click_element(CLP.commodity_loc,doc='点击商品')
        self.click_element(CLP.commodity_list_loc,doc='点击商品列表')
        self.click_element(CLP.reset_loc,doc='点击重置')
        self.click_element(CLP.enter_search_loc,doc='点击输入搜索')
        self.input_text(CLP.enter_search_loc,text=text,doc=text)
        self.click_element(CLP.query_results_loc,doc='点击查询结果')
        time.sleep(3)
        self.get_text(CLP.enter_searchassert_loc,doc='输入搜索断言元素')

    """商品名称搜索功能"""
    def product_code(self):
        time.sleep(2)
        self.click_element(CLP.commodity_loc,doc='点击商品')
        self.click_element(CLP.commodity_list_loc,doc='点击商品列表')
        self.click_element(CLP.reset_loc,doc='点击重置')
        self.click_element(CLP.product_code_loc,doc='点击货号')
        self.input_text(CLP.product_code_loc,text='6946605',doc='输入货号：6946605')
        self.click_element(CLP.query_results_loc,doc='点击查询结果')
        time.sleep(3)
        self.get_text(CLP.enter_searchassert_loc,doc='输入搜索断言元素')

    """商品品牌搜索功能"""
    def commodity_brand(self):
        time.sleep(2)
        self.click_element(CLP.commodity_loc,doc='点击商品')
        self.click_element(CLP.commodity_list_loc,doc='点击商品列表')
        self.click_element(CLP.reset_loc,doc='点击重置')
        self.click_element(CLP.commodity_brand_loc,doc='点击品牌')
        time.sleep(2)
        self.click_element(CLP.Apple_brand_loc,doc='点击苹果品牌')
        self.click_element(CLP.query_results_loc,doc='点击查询结果')
        time.sleep(3)
        self.get_text(CLP.enter_searchassert_loc,doc='输入搜索断言元素')

    """商品上架状态搜索功能"""
    def shelf_status(self):
        time.sleep(2)
        self.click_element(CLP.commodity_loc,doc='点击商品')
        self.click_element(CLP.commodity_list_loc,doc='点击商品列表')
        self.click_element(CLP.reset_loc,doc='点击重置')
        self.click_element(CLP.shelf_status_loc,doc='点击上下架按钮状态')
        time.sleep(2)
        self.click_element(CLP.down_shelf_loc,doc='点击下架按钮')
        self.click_element(CLP.query_results_loc,doc='点击查询结果')
        time.sleep(3)
        self.get_text(CLP.enter_searchassert_loc,doc='输入搜索断言元素')

    """删除道长牌T恤商品功能"""
    def delete_commodity(self,text):
        time.sleep(2)
        self.click_element(CLP.commodity_loc,doc='点击商品')
        self.click_element(CLP.commodity_list_loc,doc='点击商品列表')
        self.click_element(CLP.reset_loc,doc='点击重置')
        self.click_element(CLP.enter_search_loc,doc='点击输入搜索')
        self.input_text(CLP.enter_search_loc,text=text,doc='道长牌T恤')
        self.click_element(CLP.query_results_loc,doc='点击查询结果')
        time.sleep(3)
        self.click_element(CLP.delete_loc,doc='点击删除按钮')
        self.click_element(CLP.submit_yes_loc,doc='点击删除按钮确定')
        # self.get_text(CLP.enter_searchassert_loc,doc='输入搜索断言元素')
        # self.click_element(CLP.commodity_loc,doc='点击商品')
        # self.click_element(CLP.commodity_list_loc,doc='点击商品列表')
        # self.click_element(CLP.enter_search_loc,doc='点击输入搜索')
        # self.input_text(CLP.enter_search_loc,text='道长牌T恤',doc='道长牌T恤')
        # self.click_element(CLP.query_results_loc,doc='点击查询结果')
        # time.sleep(3)
        # self.get_text(CLP.enter_searchassert_loc,doc='输入搜索断言元素')

    # #商品货号元素
    # product_code_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[2]/form/div[2]/div/div/input')
    # #商品分类元素
    # commodity_class_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[2]/form/div[3]/div/span')
    # #商品品牌元素
    # commodity_brand_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[2]/form/div[4]/div/div/div[1]/input')
    # #上架状态元素
    # shelf_status_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[2]/form/div[5]/div/div/div[1]')
    # #审核状态元素
    # audit_status_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[2]/form/div[6]/div/div/div[1]')
    # #重置元素
    # reset_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[1]/button[2]')
    # #查询结果元素
    # query_results_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[1]/button[1]')